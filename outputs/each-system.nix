myself:

{ self
, nixpkgs
, sops-nix
, deploy-rs
, krops
, nixos-infect
, pre-commit-hooks
, ...
}:

genAttrsForNodes:

system:

let

  pkgs = import nixpkgs {
    inherit system;
    config = { allowUnfree = true; };
    overlays = [ myself.overlays.default ];
  };
  sopsPkgs = sops-nix.packages.${system};
  kropsPkgs = krops.packages.${system};
  selfPkgs = myself.legacyPackages.${system};

  inherit (pkgs) lib;

  pre-commit-check = lib.nixosFlake.mkPreCommitCheckForNix pre-commit-hooks self.outPath { };

  withDeployRsChecks = false; # DISABLED: checking all deploy confs of all nodes on each run of deploy-rs (even for a single node) is too slow; also disabled by flag in ../pkgs/deploy-rs-flake-nixos-configuration.nix; see https://github.com/serokell/deploy-rs/issues/89

in
{

  checks = (if withDeployRsChecks then (deploy-rs.lib.${system}.deployChecks self.deploy) else { }) //
    {
      inherit pre-commit-check;
    };

  devShell = lib.nixosFlake.mkMinimalShell {
    # imports all files ending in .asc/.gpg and sets $SOPS_PGP_FP
    # see https://github.com/Mic92/sops-nix
    sopsPGPKeyDirs = [
      (self.outPath + "/sops-keys/hosts")
      (self.outPath + "/sops-keys/users")
    ];

    nativeBuildInputs =
      let
        inputsPkgs = with pkgs; [
          git
          git-crypt # https://github.com/AGWA/git-crypt
          sops
        ];
        inputsSopsPkgs = with sopsPkgs; [
          ssh-to-pgp
          sops-import-keys-hook
        ];
        inputsSelfPkgs = with selfPkgs; [
          nixpkgs-fmt
          nix-linter-with-checks
        ];
      in
      inputsPkgs ++ inputsSopsPkgs ++ inputsSelfPkgs;

    shellHook = ''
      ${pre-commit-check.shellHook}
      sopsImportKeysHook  # to include shellHook from sops-import-keys-hook which would be overriden otherwise
      alias sops-binary='sops --verbose --input-type binary --output-type binary'
      alias nixos-rebuild="nixos-rebuild --flake '.#'"
    '';

  };

  # nix build '.#pkgName'
  packages =
    genAttrsForNodes true "system-" (node: self.nixosConfigurations."${node.name}".config.system.build.toplevel)
    //
    genAttrsForNodes true "tarball-" (node: self.nixosConfigurations."${node.name}".config.system.build.tarball)
    //
    genAttrsForNodes true "vm-" (node: self.nixosConfigurations."${node.name}".config.system.build.vm)
  ;

  # nix run '.#appName'
  apps =
    let

      exportSshSudoEnvForNode = node: ''
        export SSH_HOST="${node.deployRs.hostname}"
        export SSH_USER="${nixpkgs.lib.strings.optionalString (node.deployRs ? profiles.system.sshUser) node.deployRs.profiles.system.sshUser}"
        export SSH_IDENTITY="${nixpkgs.lib.strings.optionalString ((node ? sshKeyFile) && (builtins.isString node.sshKeyFile || builtins.isPath node.sshKeyFile)) node.sshKeyFile}"
        export SSH_OPTS="${nixpkgs.lib.strings.optionalString (node.deployRs ? profiles.system.sshOpts) (builtins.concatStringsSep " " node.deployRs.profiles.system.sshOpts)}"
        export SUDO_USER="${nixpkgs.lib.strings.optionalString ((node.deployRs ? profiles.system.user) && (node.deployRs.profiles.system.user != node.deployRs.profiles.system.sshUser)) node.deployRs.profiles.system.user}"
      '';

    in
    {

      # nix run '.#switch'
      switch = {
        type = "app";
        program = builtins.toString (pkgs.writeShellScript "nixos-rebuild-switch" ''
          exec ${selfPkgs.nixosFlake.nixos-rebuild-switch-flake} ${self} $@
        '');
      };

      # nix run '.#boot'
      boot = {
        type = "app";
        program = builtins.toString (pkgs.writeShellScript "nixos-rebuild-boot" ''
          exec ${selfPkgs.nixosFlake.nixos-rebuild-boot-flake} ${self} $@
        '');
      };

      # nix run '.#repl'
      repl = {
        type = "app";
        program = builtins.toString (pkgs.writeShellScript "repl-localhost" ''
          exec ${selfPkgs.nixosFlake.repl-flake-nixos-configuration} ${self} $@
        '');
      };

      # nix run '.#size'
      size = {
        type = "app";
        program = builtins.toString (pkgs.writeShellScript "result-closure-size" ''
          exec ${selfPkgs.nixosFlake.closure-size-nixos-configuration} $@
        '');
      };

      # nix run '.#graph'
      graph = {
        type = "app";
        program = builtins.toString (pkgs.writeShellScript "result-graph-viewer" ''
          exec ${selfPkgs.nixosFlake.nix-store-graph-nixos-configuration} $@
        '');
      };

      # nix run '.#sops-config-update-keys'
      sops-config-update-keys = {
        type = "app";
        program = builtins.toString selfPkgs.nixosFlake.sops-config-update-keys;
      };

      # nix run '.#sops-files-clean'
      sops-files-clean = {
        type = "app";
        program = builtins.toString selfPkgs.nixosFlake.sops-files-clean;
      };

      # nix run '.#sops-files-decrypt'
      sops-files-decrypt = {
        type = "app";
        program = builtins.toString selfPkgs.nixosFlake.sops-files-decrypt;
      };

      # nix run '.#sops-files-encrypt'
      sops-files-encrypt = {
        type = "app";
        program = builtins.toString selfPkgs.nixosFlake.sops-files-encrypt;
      };

      # nix run '.#sops-files-update-keys'
      sops-files-update-keys = {
        type = "app";
        program = builtins.toString selfPkgs.nixosFlake.sops-files-update-keys;
      };

    }

    //
    # nix run '.#vm-nodeName'
    genAttrsForNodes true "vm-" (node: {
      type = "app";
      program = builtins.toString (pkgs.writeShellScript "build-vm-${node.name}" ''
        exec ${selfPkgs.nixosFlake.nixos-rebuild-build-vm-flake-nixos-configuration} ${self} ${node.name}
      '');
    })

    //
    # nix run '.#repl-nodeName'
    genAttrsForNodes true "repl-" (node: {
      type = "app";
      program = builtins.toString (pkgs.writeShellScript "repl-${node.name}" ''
        exec ${selfPkgs.nixosFlake.repl-flake-nixos-configuration} ${self} ${node.name} $@
      '');
    })

    //
    # nix run '.#ssh-nodeName'
    genAttrsForNodes (node: node ? deployRs) "ssh-" (node: {
      type = "app";
      program = builtins.toString (pkgs.writeShellScript "ssh-${node.name}" ''
        echo
        echo "# Connecting via SSH to ${node.name} (the prompt may not be visible) ..." >&2
        # run the shell specified by the target user's password database entry as a login shell
        ${exportSshSudoEnvForNode node}
        exec ${selfPkgs.nixosFlake.ssh-with-sudo} $@
      '');
    })

    //
    # nix run '.#deploy-rs-nodeName'
    genAttrsForNodes (node: self.deploy.nodes ? ${node.name}) "deploy-rs-" (node: {
      type = "app";
      program = builtins.toString (pkgs.writeShellScript "deploy-rs-${node.name}" ''
        exec ${selfPkgs.nixosFlake.deploy-rs-flake-nixos-configuration} ${self} ${node.name} $@
      '');
    })

    //
    # nix run '.#deploy-krops-nodeName'
    genAttrsForNodes (node: node ? deployRs) "deploy-krops-" (node:
      let
        targetPath = "/root";  # using /root here as the configuration repo will be copied there including confidential files such as SSH keys
        sshPortFromOpts = sshOpts: builtins.match ".*-p[[:space:]]+([0-9]+).*" (builtins.concatStringsSep " " sshOpts);
        kropsCmd = kropsPkgs.writeCommand "deploy-krops-cmd-${node.name}" {
          source = krops.lib.evalSource [{
            nixos.file = {
              path = builtins.toString self.outPath;
              useChecksum = true;
            };
          }];
          target = {
            path = targetPath;
            extraOptions = (node.deployRs.profiles.system.sshOpts or [ ]) ++ nixpkgs.lib.lists.optionals (node ? sshKeyFile) [
              "-i"
              (builtins.toString node.sshKeyFile)
            ];
            host = node.deployRs.hostname;
            # ssh port must be defined separately in krops, so we extract that from sshOpts; must be a string, not a number
            port = if (node.deployRs ? profiles.system.sshOpts) && (sshPortFromOpts node.deployRs.profiles.system.sshOpts != null) then (sshPortFromOpts node.deployRs.profiles.system.sshOpts) else "22";
            # assertion: the krops sudo does not allow to specify the sudo user, so the sudo user must be root which is almost equivalent to sudo without the username specified
            sudo = (node.deployRs ? profiles.system.user) && (node.deployRs.profiles.system.user != node.deployRs.profiles.system.sshUser) && (assert (node.deployRs.profiles.system.user == "root"); true);
          } // (if ! node.deployRs ? profiles.system.sshUser then { } else {
            user = node.deployRs.profiles.system.sshUser;
          });
          # NOTE: the very first word of the following script is utilized to name the script running in nix-store, so it must be a valid filename part
          # run two-times: sometimes services fail to switch on the first run, but are fine on the second
          command = targetPath: ''
            nix-shell -p git nixFlakes --run 'nixos-rebuild --verbose switch --flake "${targetPath}/nixos#${node.name}" -L --keep-going || nixos-rebuild --verbose switch --flake "${targetPath}/nixos#${node.name}" -L --keep-going'
          '';
        };
      in
      {
        type = "app";
        program = builtins.toString (pkgs.writeShellScript "deploy-krops-${node.name}" ''
          echo
          echo "# Creating krops sentinel file ${node.deployRs.hostname}:${targetPath}/.populate ..." >&2
          # do not know why there is true as the first command but otherwise, the mkdir argument would be ignored
          ${exportSshSudoEnvForNode node}
          ${selfPkgs.nixosFlake.ssh-with-sudo} sh -c 'true && mkdir -vp "${targetPath}" && touch "${targetPath}/.populate"'
          echo
          echo "# Running krops script ${kropsCmd} ..." >&2
          exec ${kropsCmd}
        '');
      })

    # TODO: implement also local/remote building and remote activation directly by nixos-rebuild; see # see https://zimbatm.com/NixFlakes/#building-nixos-configurations-with-flakes

    //
    # nix run '.#infect-nodeName'
    genAttrsForNodes (node: node ? deployRs && node ? nixosInfectResources) "infect-" (node:
      let
        targetNixChannel = "nixos-21.11";
        targetNixOsImport = "/etc/nixos/nixos-infect-resources";
        inherit (pkgs) gnutar;
        resourcesTar = pkgs.runCommand "nixos-infect-resources-${node.name}.tar" { } ''
          mkdir -p ./resources
          echo "# copying nixos-infect resources ..." >&2
          for I in ${builtins.concatStringsSep " " node.nixosInfectResources}; do
            cp -v -r --no-preserve=mode "$I" ./resources/$(stripHash "$I")
          done
          echo "# making a TAR for the copied nixos-infect resources ..." >&2
          ${gnutar}/bin/tar -v -C ./resources --exclude "*.sh" -cf $out "."
        '';
      in
      {
        type = "app";
        program = builtins.toString (pkgs.writeShellScript "infect-${node.name}" ''
          # halt on any error
          set -e
          ${exportSshSudoEnvForNode node}
          # deploy nixos-infect
          echo "# NOTE: Before the infection, check ./deploy-rs.nix for correct sshUser value which may differ from the production NixOS SSH user." >&2
          echo
          echo "# Deploying nixos-infect script to ${node.deployRs.hostname}:/tmp ..." >&2
          cat ${nixos-infect}/nixos-infect \
          | ${selfPkgs.nixosFlake.ssh-with-sudo} 'sh -c "hostname -f && cat > /tmp/nixos-infect && chmod 755 /tmp/nixos-infect && ls -l /tmp/nixos-infect"'
          # deploy configuration with ssh-keys
          echo
          echo "# Depolying ${resourcesTar} (${builtins.concatStringsSep ";" node.nixosInfectResources}) to ${node.deployRs.hostname}:${targetNixOsImport} ..." >&2
          cat ${resourcesTar} \
          | ${selfPkgs.nixosFlake.ssh-with-sudo} 'sh -c "hostname -f && mkdir -vp ${targetNixOsImport} && tar -C ${targetNixOsImport} -xv --overwrite && ls -l ${targetNixOsImport}"'
          # start the infection
          echo
          echo "# Infecting ${node.deployRs.hostname} with NixOS ${targetNixChannel} and ${targetNixOsImport} ..." >&2
          echo "# * or just print a NixOS version if already running the NixOS system" >&2
          echo "# * after a reboot of the infected system, remove the original root directory by: rm -rf /old-root" >&2
          exec ${selfPkgs.nixosFlake.ssh-with-sudo} 'sh -c "hostname -f && nixos-version --json || NIX_CHANNEL=${targetNixChannel} NIXOS_IMPORT=${targetNixOsImport} exec bash -x /tmp/nixos-infect"'
        '');
      })

  ;

}
