{

  description = "Nix flake library for NixOS configuration and deployment.";

  nixConfig = {
    # https://nixos.org/manual/nix/unstable/command-ref/conf-file.html
    bash-prompt-suffix = "dev-shell# ";
  };

  inputs = {

    nixpkgs.url = "nixpkgs/release-22.05";

    nixpkgs-unstable.url = "nixpkgs/master";

    utils.url = "github:numtide/flake-utils";

    home-manager = {
      url = "github:nix-community/home-manager/release-22.05";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    sops-nix = {
      url = "github:Mic92/sops-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # deployment mgmt: for local build and remote deployment by copying the build artifacts and activation scripts
    deploy-rs = {
      # https://github.com/serokell/deploy-rs
      url = "github:serokell/deploy-rs";
      inputs.nixpkgs.follows = "nixpkgs"; # this wont have any effect probably as we are overlaying deploy-rs in nixpkgs
      inputs.utils.follows = "utils";
    };

    # deployment mgmt: for remote build and remote deployment by copying the NixOS configuration
    krops = {
      # https://github.com/Mic92/krops
      url = "github:Mic92/krops";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "utils";
    };

    # deployment mgmt: for remote infection of non-NixOS system by NixOS with an initial NixOS configuration
    nixos-infect = {
      # https://github.com/elitak/nixos-infect
      url = "github:elitak/nixos-infect";
      flake = false;
    };

    # QA checks of the source code
    pre-commit-hooks = {
      # https://github.com/cachix/pre-commit-hooks.nix
      url = "github:cachix/pre-commit-hooks.nix";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "utils";
    };

  };

  outputs = { self, nixpkgs, utils, deploy-rs, pre-commit-hooks, ... } @ args:
    let

      defaultSystems =
        let
          excludedSystems = [
            "x86_64-darwin" # for `hostname` uses old nettools-1003.1-2008 which requires insecure openssl-1.0.2u
            "i686-linux" # package dotnet-sdk-6.0.300 is not available for this platform (required by checks)
          ];
        in
        nixpkgs.lib.lists.subtractLists excludedSystems utils.lib.defaultSystems;

      mkOutputsWithInputs = import ./outputs self;
      mkOutputsWithAdditionalInputs = additionalInputs: mkOutputsWithInputs (args // additionalInputs);
      mkOutputs = mkOutputsWithInputs args;

      legacyPackagesForPkgs = pkgs: {
        inherit (pre-commit-hooks.packages.${pkgs.system}) nixpkgs-fmt;
        nix-linter-with-checks = pkgs.callPackage ./pkgs/nix-linter-with-checks { inherit (pre-commit-hooks.packages.${pkgs.system}) nix-linter; };
        nixosFlake = pkgs.lib.recurseIntoAttrs {
          closure-size-nixos-configuration = pkgs.callPackage ./pkgs/closure-size-nixos-configuration.nix { };
          deploy-rs-flake-nixos-configuration = pkgs.callPackage ./pkgs/deploy-rs-flake-nixos-configuration.nix { }; # deploy-rs is adopted from overlays
          nixos-rebuild-boot-flake = pkgs.callPackage ./pkgs/nixos-rebuild-boot-flake.nix { };
          nixos-rebuild-build-vm-flake-nixos-configuration = pkgs.callPackage ./pkgs/nixos-rebuild-build-vm-flake-nixos-configuration.nix { };
          nixos-rebuild-switch-flake = pkgs.callPackage ./pkgs/nixos-rebuild-switch-flake.nix { };
          nix-store-graph-nixos-configuration = pkgs.callPackage ./pkgs/nix-store-graph-nixos-configuration.nix { };
          repl-flake-nixos-configuration = pkgs.callPackage ./pkgs/repl-flake-nixos-configuration.nix { };
          sops-config-update-keys = pkgs.callPackage ./pkgs/sops-config-update-keys.nix { };
          sops-files-clean = pkgs.callPackage ./pkgs/sops-files-clean.nix { };
          sops-files-decrypt = pkgs.callPackage ./pkgs/sops-files-decrypt.nix { };
          sops-files-encrypt = pkgs.callPackage ./pkgs/sops-files-encrypt.nix { };
          sops-files-update-keys = pkgs.callPackage ./pkgs/sops-files-update-keys.nix { };
          ssh-with-sudo = pkgs.callPackage ./pkgs/ssh-with-sudo.nix { };
        };
      };

    in
    {

      lib = {
        inherit defaultSystems mkOutputsWithInputs mkOutputsWithAdditionalInputs mkOutputs;
      };

      # an overlay to extend the pkgs.lib by additional functions
      overlays.default = final: prev: {
        lib = prev.lib // {
          nixosFlake = import ./lib.nix {
            pkgs = prev;
            selfPkgs = self.legacyPackages.${prev.system};
            inherit (prev) lib;
          };
        };
      } // legacyPackagesForPkgs prev;

    } // utils.lib.eachSystem defaultSystems (system:
      let

        pkgs =
          let
            # deploy-rs overlay is split to deploy-rs and lib so we need to dive into deploy-rs to get a valid overlay
            # TODO: switch to deploy-rs.overlays.default as soon as it implements a new Flake output schema; see https://github.com/serokell/deploy-rs/blob/master/flake.nix
            deployRsOverlay = final: prev: { inherit ((deploy-rs.overlay final prev).deploy-rs) deploy-rs; };
          in
          import nixpkgs {
            inherit system;
            overlays = [
              self.overlays.default
              deployRsOverlay
            ];
          };

        inherit (pkgs) lib;

        pre-commit-check = lib.nixosFlake.mkPreCommitCheckForNix pre-commit-hooks self.outPath { };

        legacyPackages = legacyPackagesForPkgs pkgs;

        packages = utils.lib.flattenTree legacyPackages;

      in
      {

        inherit legacyPackages packages;

        checks = {
          inherit pre-commit-check;
          libs-seq-check = lib.nixosFlake.mkSeqCheck [
            self.lib
            lib.nixosFlake
          ];
        };

        devShells.default = lib.nixosFlake.mkMinimalShell {
          nativeBuildInputs = with pkgs; [
            nixpkgs-fmt
            packages.nix-linter-with-checks
          ];
          inherit (pre-commit-check) shellHook;
        };

      });

}
