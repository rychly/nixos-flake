{ lib
, writeShellScriptBin
, nix-linter
, nixLinterChecks ? { }
}:

let

  name = "nix-linter-with-checks";

  defaultChecks = {
    # see https://github.com/Synthetica9/nix-linter/tree/master/examples
    FreeLetInFunc = false; # DISABLED: as the arguments should be defined as the first thing in the function to provide its documentation
    UnfortunateArgName = false; # DISABLED: needed for overlays in nix flake which must be (final: prev:)
    UnusedArg = false; # DISABLED: needed for overlays in nix flake which must be (final: prev:)
    BetaReduction = true;
    UnneededAntiquote = true; # ENABLED: to replace "${x}" by (builtins.toString x)
  };

  finalChecks = defaultChecks // nixLinterChecks;

  checkArgsList = lib.attrsets.mapAttrsToList (name: value: if value then "--check=${name}" else "--check=no-${name}") finalChecks;

in

writeShellScriptBin name ''
  exec ${nix-linter}/bin/nix-linter ${builtins.concatStringsSep " " checkArgsList} $@
''
