{ writeShellScript
}:

let

  name = "nixos-rebuild-switch-flake";

in
writeShellScript "${name}.sh" ''

  if [ "$1" = "--help" ]; then
    echo "Usage: $0 <flake-path>" >&2
    exit 1
  fi

  flakePath="$1"
  shift

  if ! [ -r "$flakePath" ]; then
    echo "The first argument must by a valid Flake path!" >&2
    exit 2
  fi

  echo "# Switching the localhost nixosConfigurations from $flakePath ..." >&2
  exec sudo su -c "http_proxy= https_proxy= exec nixos-rebuild \
  --verbose --flake '$flakePath' switch -L $@"

''
