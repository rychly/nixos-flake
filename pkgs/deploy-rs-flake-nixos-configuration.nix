{ writeShellScript
, inetutils
, deploy-rs
}:

let

  name = "deploy-rs-flake-nixos-configuration";

in
writeShellScript "${name}.sh" ''

  if [ "$1" = "--help" ]; then
    echo "Usage: $0 <flake-path> [nixos-configuration-name]" >&2
    exit 1
  fi

  flakePath="$1"
  shift

  nixosConfigurationName="$1"
  shift

  if ! [ -r "$flakePath" ]; then
    echo "The first argument must by a valid Flake path!" >&2
    exit 2
  fi

  if [ -z "$nixosConfigurationName" ]; then
    nixosConfigurationName=$(${inetutils}/bin/hostname -s)
    echo "Setting name of the nixosConfiguration to default hostname: $nixosConfigurationName" >&2
  fi

  echo
  echo "# Building the nixosConfigurations.$nixosConfigurationName from $flakePath locally and deploying the result by deploy-rs ..." >&2
  # skip the automatic pre-build checks until they wont be checking also unrelated nixosConfiguration; see https://github.com/serokell/deploy-rs/issues/89
  exec ${deploy-rs}/bin/deploy --skip-checks "$flakePath#$nixosConfigurationName" $@ -- -L

''
